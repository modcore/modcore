extern crate libloading;

mod modcore_headers;
use modcore_headers::*;

use std::iter::Iterator;
use std::any::Any;
use std::sync::{Arc,Mutex};
use std::sync::atomic::{AtomicUsize,Ordering};
use std::sync::mpsc::{channel,Sender};

struct MyData {
    tx: Mutex<Sender<isize>>
}

fn my_notify(data: Arc<dyn Any + Send + Sync>,event: ModNotif) {
    let data = Arc::downcast::<MyData>(data).unwrap();
    let tx = data.tx.lock().unwrap();
    match event {
        ModNotif::Init(_) => tx.send(1),
        ModNotif::Deinit(_) => tx.send(-1)
    };
}

fn main() {
    let appstate = AppState::new();
    
    let mut args=std::env::args();
    args.next();

    let mut to_load = vec!();

    for arg in args {
        if &arg[..2]=="--" {
            match &arg[..] {
                "--help" => {
                    println!("Usage: modcore MOD1 MOD2 ...");
                    return;
                },
                _ => {
                    panic!("Invalid argument");
                }
            }
        } else {
            unsafe {
                let mlib = Box::leak(Box::new(libloading::Library::new(std::path::Path::new(&arg[..]).canonicalize().expect(Box::leak(format!("Failed to resolve path: {}",arg).into_boxed_str()))).expect(Box::leak(format!("Failed to load dylib: {}",arg).into_boxed_str()))));
                let func: libloading::Symbol<unsafe extern fn(AppStateRaw)> = mlib.get(b"modcore_init").expect(Box::leak(format!("Dylib is not a mod: {}",arg).into_boxed_str()));
                to_load.push((arg,func));
            }
        }
    }

    eprintln!("ModCore Alpha v2021.09.03");

    let (tx,rx) = channel::<isize>();
    let data = Arc::new(MyData { tx: Mutex::new(tx) });
    let nh=appstate.modinit_notify(Arc::clone(&data) as Arc<dyn Any + Send + Sync>,Box::new(my_notify));

    for (name,func) in to_load {
        unsafe {
            eprintln!("Loading mod: {}", name);
            (func)(appstate.clone().into());
        }
    }

    let mut counter: isize = 0;
    
    loop {
        let inc=rx.recv().unwrap();
        counter=counter+inc;
        if counter>0 {
            break;
        }
    }

    loop {
        let inc=rx.recv().unwrap();
        counter=counter+inc;
        if counter<=0 {
            break;
        }
    }
    std::mem::drop(nh);
}
