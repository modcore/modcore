mod modcore_headers;
use modcore_headers::*;

#[no_mangle]
unsafe extern fn modcore_init(appstate: AppStateRaw) {
    let svt = SomeValueTypes::new();
    my_mod::my_main(appstate.into(),svt)
}

mod my_mod;
