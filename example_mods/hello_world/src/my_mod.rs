use crate::modcore_headers::*;
use std::any::Any;
use std::sync::{Arc,Mutex};
use std::collections::HashMap;

struct MyData {
    svt: SomeValueTypes,
    mih: Mutex<Option<NotifHandle>>,
    mh: Mutex<Option<ModHandle>>
}

fn my_call_func(data: Arc<dyn Any + Send + Sync>, name: &str, arg: SomeValue) -> SomeValue {
    let data = Arc::downcast::<MyData>(data).unwrap();
    match name {
        "hello_world" => {
            println!("Hello world!");
        },
        _ => {
            panic!("func {} not defined",name);
        }
    }
    data.svt.make_null()
}

fn my_get_features(_data: Arc<dyn Any + Send + Sync>) -> HashMap<String,u64> {
    let mut hmm = HashMap::new();
    hmm.insert("fn hello_world(null) -> null".to_owned(),0);
    hmm
}

fn drop_notif(data: Arc<MyData>) {
    loop {
        let mut mih = data.mih.lock().unwrap();
        if mih.is_some() {
            let nh=mih.take().unwrap();
            break;
        }
        std::thread::sleep(std::time::Duration::from_millis(10));
    }
}

fn drop_mh(data: Arc<MyData>) {
    loop {
        let mut mh = data.mh.lock().unwrap();
        if mh.is_some() {
            let mh = mh.take().unwrap();
            break;
        }
        std::thread::sleep(std::time::Duration::from_millis(10));
    }
}

fn my_modinit_notify(data: Arc<dyn Any + Send + Sync>, event: ModNotif) {
    let data = Arc::downcast::<MyData>(data).unwrap();
    match event {
        ModNotif::Init(ms) => {
            let ms=ms.clone();
            if ms.get_name()=="hello_world" {
                ms.call("hello_world",data.svt.make_null());
                let dt1=Arc::clone(&data);
                std::thread::spawn(move || {
                    drop_notif(dt1);
                });
                for i in (0..=10).rev() {
                    std::thread::sleep(std::time::Duration::from_millis(1000));
                    if i==0 {
                        println!("Exit!");
                    } else {
                        println!("Exiting in {}",i);
                    }
                }
                let dt2=Arc::clone(&data);
                std::thread::spawn(move ||{
                    drop_mh(dt2);
                });
            }
        },
        _ => ()
    }
}

pub fn my_main(appstate: AppState, svt: SomeValueTypes) {
    let data = Arc::new(MyData { svt, mih: Mutex::new(None), mh: Mutex::new(None) });
    let nh=appstate.modinit_notify(Arc::clone(&data) as Arc<dyn Any + Send + Sync>,Box::new(my_modinit_notify));
    data.mih.lock().unwrap().replace(nh);
    let mh=appstate.register_mod("hello_world".into(),Arc::clone(&data) as Arc<dyn Any + Send + Sync>,Arc::new(my_call_func),Arc::new(my_get_features));
    data.mh.lock().unwrap().replace(mh);
}
